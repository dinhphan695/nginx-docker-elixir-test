defmodule NGINXDOCKERELIXIR do
  @moduledoc """
  Documentation for `NGINXDOCKERELIXIR`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> NGINXDOCKERELIXIR.hello()
      :world

  """
  def hello do
    :world
  end
end
